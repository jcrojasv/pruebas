const editLossFactorsTable = Vue.component('edit-loss-factors-table', {
  template: `
    <div class="edit-loss-factors-table">
      <div :class="{'block-div': showModal}"></div>
      <div class="button-edit-table" @click="showModal = !showModal">
        <img src="assets/images/icon-edit-table-loss-factors.svg">
      </div>
      <transition name="slide">
        <div class="edit-table-container" v-show="showModal">
          <div class="edit-table-row">
            <div class="image-header"><img src="assets/images/icon-edit-loss-factor.svg"></div>
            <div class="text-header">Editar detalle de pérdidas</div>
          </div>

          <div class="edit-table-row bg-blue text-color-header">
            <div class="row-first">Ordenar</div>
            <div class="row-others">
              Columnas
            </div>
            <div class="row-others text-center">Visibilidad</div>
          </div>

          <div class="edit-table-row border-bottom-edit">
            <div class="row-first position-relative">
              <input min=0 type="number" class="form-control" disabled="true" value="0">
              <img src="assets/images/icon-arrow-disabled.svg" class="arrow-disabled">
            </div>
            <div class="row-others more-info">
              <span class="text">Selector</span>
              <span
                class="image"
                data-toggle="tooltip"
                data-placement="bottom"
                data-html="true"
                title="Al hacer click permite seleccionar registros
                individuales para exportar."
                >
                <img src="assets/images/icon-more-info.svg">
              </span>
            </div>
            <div class="row-others text-center opacity">
              <img src="assets/images/icon-eye-loss-factors.svg">
            </div>
          </div>

          <draggable
            :list="columnShowList"
            v-bind="dragOptions"
            >
            <transition-group type="transition" name="flip-list">
              <div class="edit-table-row border-bottom-edit" v-for="row, index in columnShowList" :key="index">
                <div class="row-first position-relative">
                  <input min=0 max=3
                    type="number"
                    class="form-control order"
                    :name="row.id + '-' + index"
                    :value="row.order"
                  >
                  <img src="assets/images/icon-input-arrow-up.svg" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="input-up">
                  <img src="assets/images/icon-input-arrow-down.svg" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="input-down">
                </div>
                <div class="row-others">{{ row.title }}</div>
                <div class="row-others text-center cursor-pointer"
                  @click="changeShowColumn(index, row)"
                  :class="{ 'opacity': !row.show }">
                  <img src="assets/images/icon-eye-loss-factors.svg">
                </div>
              </div>
            </transition-group>
          </draggable>

          <div class="refresh-section">
            <img src="/assets/images/icon-refresh-loss-factors.svg" @click="refresh()">
            <span @click="refresh()">Reestablecer orden original</span>
          </div>

          <div class="buttons">
            <button class="btn btn-light" @click="showModal=false">Cerrar</button>
            <button class="btn btn-light" @click="applyChanges">Aplicar</button>
          </div>

          <div class="error-message" v-if="errorMessage">
            <span>{{ errorMessage }}</span>
          </div>
        </div>
      </transition>
    </div>
  `,
  props: {
    columnList: {
      type: Array,
      required: true,
    }
  },
  data: function () {
    return {
      columnResult: this.columnList.slice(),
      errorMessage: '',
      minOrderCant: 3,
      showModal: false,
    }
  },
  methods: {
    applyChanges: function () {
      this.updateOrder();
      if (!this.cantOrderValidate()) {
        console.log('Hola Jorge');
        this.$emit('set-column-order', this.columnResult);
      }
    },
    close: function () {
      this.showModal = false;
    },
    refresh: function () {
      this.columnShowList = this.$parent.columnOriginalList;
      console.log(this.columnShowList, 'El show list deberia cambiar');
    },
    changeShowColumn: function (index, column) {
      console.log(index, row, this.columnResult);
      this.columnResult[index].show = this.columnResult[index].show ? false : true;
    },
    updateOrder: function () {
      const el = document.getElementsByClassName('form-control order');
      for (let i = 0; i < el.length; i++) {
        this.columnResult[parseInt(el[i].name.split('-')[1])].order = el[i].value;
      }
    },
    validateShow: function () {
      const minBreakdown = 1;
      const minLoss = 1;
      let contBreakdown = 0;
      let contLoss = 0;
      const vm = this;

      vm.columnResult.map((column, index) => {
        if (!column.show) {
          if (column.type == 'breakdown') {
            contBreakdown += 1;
            this.columnResult[index]['error'] = contBreakdown > 1 ? true : false;
          } else if (column.type == 'loss') {
            contLoss += 1;
            this.columnResult[index]['error'] = contLoss > 1 ? true : false;
          }
        }
      });
      console.log(this.columnResult);
      return !(contBreakdown || contLoss);
    },
    cantOrderValidate: function () {
      const cont = this.columnResult.reduce((cont, el) => {
        return cont += el.order ? 1 : 0;
      }, 0);

      this.errorMessage = cont > this.minOrderCant ? '¡Orden no válido!' : '';
      return cont;
    },
    validateOrderDuplicated: function () {
      const elements = this.columnResult.reduce((column) => {
        return column.order;
      })
    }
  },
  computed: {
    dragOptions() {
      return {
        animation: 500,
        disabled: false,
      };
    },
    columnShowList: {
      get () {
        return this.columnResult.slice();
      },
      set (newValues) {
        return newValues;
      }
    }
  }
});